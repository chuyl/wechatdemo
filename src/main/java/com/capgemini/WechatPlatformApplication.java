package com.capgemini;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author chuyl
 * @date 11/21/2018
 */
@EnableScheduling
@SpringBootApplication
public class WechatPlatformApplication {

    public static void main(String[] args) {
        SpringApplication.run(WechatPlatformApplication.class, args);
    }
}
