package com.capgemini.platform.wechat.po;

/**
 * @author chuyl
 * @date 11/28/2018
 */
public class CustomerServerInfo {

    private String openid;

    private String name;

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
