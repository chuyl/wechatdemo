package com.capgemini.platform.wechat.po;

/**
 *
 * @Author:yanlchu
 * @Date:1/29/2019
 */
public class PsRequest {

    private String conversionId;

    private String question;

    public String getConversionId() {
        return conversionId;
    }

    public void setConversionId(String conversionId) {
        this.conversionId = conversionId;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }
}
