package com.capgemini.platform.wechat.utils;

import com.capgemini.platform.wechat.po.TextMessage;
import com.thoughtworks.xstream.XStream;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author chuyl
 * @date 11/8/2018
 */
public class MessageUtil {

    public static final String MESSAGE_TEXT="text";
    public static final String MESSAGE_IMAGE="image";
    public static final String MESSAGE_VOICE="voice";
    public static final String MESSAGE_VIDEO="video";
    public static final String MESSAGE_LINK="link";
    public static final String MESSAGE_LOCATION="location";
    public static final String MESSAGE_EVENT="event";
    public static final String MESSAGE_SUBSCRIBE="subscribe";
    public static final String MESSAGE_UNSUBSCRIBE="unsubscribe";
    public static final String MESSAGE_CLICK="CLICK";
    public static final String MESSAGE_VIEW="VIEW";
    /**
     * xml转为map集合
     * @param request
     * @return
     * @throws IOException
     * @throws DocumentException
     */
    public static Map<String,String> xmlToMap(HttpServletRequest request) throws IOException, DocumentException {
        Map<String,String> map =new HashMap<>();

        SAXReader reader=new SAXReader();
        InputStream ins=request.getInputStream();
        Document doc = reader.read(ins);
        Element root = doc.getRootElement();

        List<Element> list = root.elements();
        list.forEach(a->{
            map.put(a.getName(),a.getText());
        });
        ins.close();
        return map;
    }

    /**
     * 将文本消息转为xml
     * @param textMessage
     * @return
     */
    public static String textMessageToXml(TextMessage textMessage){
        XStream xStream=new XStream();
        xStream.alias("xml",textMessage.getClass());
        return xStream.toXML(textMessage);
    }

    public static String initText(String toUserName,String fromUserName,String content){
        TextMessage textMessage=new TextMessage();
        textMessage.setFromUserName(toUserName);
        textMessage.setToUserName(fromUserName);
        textMessage.setMsgType(MESSAGE_TEXT);
        textMessage.setCreateTime(String.valueOf(System.currentTimeMillis()));
        textMessage.setContent(content);
        return textMessageToXml(textMessage);
    }

    /**
     * 主菜单
     * @return
     */
    public static String menuText(){
        StringBuffer sb= new StringBuffer();
        sb.append("请按照菜单提示操作:\n\n");
        sb.append("1,超级赛亚人\n");
        sb.append("2,SLAMDUNK\n\n");
        sb.append("?,回复？调出次菜单。");
        return sb.toString();
    }

    public static String firstMenu(){
        StringBuffer sb= new StringBuffer();
        sb.append("超级赛亚人蓝色和桃红");
        return sb.toString();
    }

    public static String secondMenu(){
        StringBuffer sb= new StringBuffer();
        sb.append("湘北大山王");
        return sb.toString();
    }

    public static void main(String args[]){
        TextMessage a=new TextMessage();
        a.setContent("adad");
        a.setMsgType("12");
        String s = MessageUtil.textMessageToXml(a);
        System.out.println(s);
    }


    
}
