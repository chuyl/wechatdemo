package com.capgemini.platform.wechat.utils;

import it.sauronsoftware.jave.*;

import java.io.*;

/**
 * 转换格式工具类
 * @Author:yanlchu
 * @Date:12/4/2018
 */
public class ConvertAudio {

    public static void Amr2Wav(File source,File target){

        String system= System.getProperty("os.name").toUpperCase();
        if(system.startsWith("WINDOW")){
            Amr2WavForWindows(source,target);
        }else{
            Amr2WavForLinux(source,target);
        }

    }

    private static void Amr2WavForLinux(File source, File target) {
        String sourcePath =source.getAbsolutePath();
        String targetPath= target.getAbsolutePath();

        String command = "ffmpeg -i "+sourcePath+" -ar 16000 -ac 1 -y -ab 16k "+targetPath;
        Runtime runtime = Runtime.getRuntime();
        try {
            Process proc = runtime.exec(command);
            InputStream stderr = proc.getErrorStream();
            InputStreamReader isr = new InputStreamReader(stderr);
            BufferedReader br = new BufferedReader(isr);
            String line;
            StringBuffer sb = new StringBuffer();
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            int exitVal = proc.waitFor();
            System.out.println("the exitVal is : "+exitVal);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            System.out.println("ffmpeg exec cmd Exception " + e.toString());
        }


    }

    private static void Amr2WavForWindows(File source, File target) {

        try {
            long amrDuration = getAmrDuration(source);
            Float abc= Float.parseFloat(Long.toString(amrDuration));
            AudioAttributes audio = new AudioAttributes();
            Encoder encoder = new Encoder();
            //设置解码格式，比特率，位数，声道等信息
            audio.setCodec("pcm_s16le");
            audio.setBitRate(new Integer(16000));
            audio.setChannels(new Integer(1));
            audio.setSamplingRate(new Integer(16000));
            audio.setVolume(new Integer(500));
            EncodingAttributes attrs = new EncodingAttributes();
            attrs.setFormat("wav");
            attrs.setAudioAttributes(audio);
            attrs.setDuration(abc);
            encoder.encode(source, target, attrs);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (
                InputFormatException e) {
            e.printStackTrace();
        } catch (
                EncoderException e) {
            e.printStackTrace();
        } catch (
                IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 得到amr的时长
     *
     * @param file
     * @return
     * @throws IOException
     */
    public static long getAmrDuration(File file) throws IOException {
        long duration = -1;
        int[] packedSize = { 12, 13, 15, 17, 19, 20, 26, 31, 5, 0, 0, 0, 0, 0, 0, 0 };
        RandomAccessFile randomAccessFile = null;
        try {
            randomAccessFile = new RandomAccessFile(file, "rw");
            long length = file.length();//文件的长度
            int pos = 6;//设置初始位置
            int frameCount = 0;//初始帧数
            int packedPos = -1;
            /////////////////////////////////////////////////////
            byte[] datas = new byte[1];//初始数据值
            while (pos <= length) {
                randomAccessFile.seek(pos);
                if (randomAccessFile.read(datas, 0, 1) != 1) {
                    duration = length > 0 ? ((length - 6) / 650) : 0;
                    break;
                }
                packedPos = (datas[0] >> 3) & 0x0F;
                pos += packedSize[packedPos] + 1;
                frameCount++;
            }
            /////////////////////////////////////////////////////
            duration += frameCount * 20;//帧数*20
        } finally {
            if (randomAccessFile != null) {
                randomAccessFile.close();
            }
        }
        return duration;
    }

}
