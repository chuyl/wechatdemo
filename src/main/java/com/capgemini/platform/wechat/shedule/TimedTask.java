package com.capgemini.platform.wechat.shedule;


import com.capgemini.platform.wechat.biz.HttpService;
import com.capgemini.platform.wechat.biz.OpenService;
import com.capgemini.platform.wechat.domain.AuthorizationInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * @author: yyb
 * @date: 17-9-7
 */
@Component
public class TimedTask {

    private static Logger logger = LoggerFactory.getLogger(TimedTask.class);

    @Autowired
    MongoTemplate mongoTemplate;
    @Autowired
    OpenService openService;
    @Autowired
    HttpService httpService;

    private final String KF_DISCONNECT_MESSAGE ="长时间未回复，链接已断开";
    private final String DISCONNECT_MESSAGE="您好，由于长时间未收到您的消息，该会话已经切换为机器人服务，您可以再次输入\"人工客服\"切换人工服务";
    private volatile LinkedBlockingQueue<AuthorizationInfo> queue = new LinkedBlockingQueue<>(300);

    private volatile boolean isEnd = false;

    @Scheduled(fixedDelay = 3600000L)
    public void refreshAccessToken() {
        // TODO 这里要取刷新 AuthorizationInfo 中的accessToken
        logger.info("-->> 开始刷新refreshAccessToken");
        isEnd = false;
        for (int i = 0; i < 10; i++) {
            new Thread(() -> {
                while (!Thread.currentThread().isInterrupted()) {
                    try {
                        if (queue.size() == 0 && isEnd) {
                            break;
                        }
                        AuthorizationInfo info = queue.poll(5, TimeUnit.SECONDS);
                        if (info != null) {
                            openService.refresh(info);
                        }
                    } catch (InterruptedException e) {
                        logger.error("刷新token错误:", e);
                    }
                }
                logger.info("更新完毕:" + Thread.currentThread().getName());
            }, "doRefresh-" + i).start();
        }

        new Thread(() -> {
            try {
                List<AuthorizationInfo> list = mongoTemplate.findAll(AuthorizationInfo.class);
                logger.info("-->> 更新token:" + list.size());
                for (AuthorizationInfo authorizationInfo : list) {
                    queue.put(authorizationInfo);
                }
            } catch (InterruptedException e) {
                logger.error("put认证信息错误:", e);
            } finally {
                isEnd = true;
            }
        }, "put-authorizationInfo").start();
    }

    @Scheduled(initialDelay= 1800000,fixedDelay = 1800000L)
    public void refresh(){
        OpenService.getTaskMap().forEach((kfId, vaMap)->{
            Long now = System.currentTimeMillis();
            Long before = (Long) vaMap.get("time");
            if((now-before)>3600000L){
                String appId= vaMap.get("appId").toString();
                String token = httpService.getToken(appId);

                httpService.sendTextMsg(token,KF_DISCONNECT_MESSAGE,kfId);

                String fsId= OpenService.kfMap.get(kfId);
                if(fsId!=null){
                    httpService.sendTextMsg(token,DISCONNECT_MESSAGE,fsId);
                    OpenService.fsMap.remove(fsId);
                    OpenService.getOnkfSet().remove(fsId);
                    OpenService.msgCacheMap.remove(fsId);
                }
                OpenService.kfMap.remove(kfId);
            }
            OpenService.getTaskMap().remove(kfId);
        });
    }
}
