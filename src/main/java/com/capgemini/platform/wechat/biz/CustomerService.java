package com.capgemini.platform.wechat.biz;

import com.capgemini.platform.wechat.po.CustomerServerInfo;
import com.mongodb.client.result.DeleteResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author chuyl
 * @date 11/28/2018
 */
@Service
public class CustomerService {

    @Autowired
    MongoTemplate mongoTemplate;

    public String addCustomerServer(CustomerServerInfo info){

        if(checkExists(info)){
            return "该数据已存在";
        }

        if(info.getOpenid()!=null){
            mongoTemplate.save(info);
        }
        return "success";
    }

    private boolean checkExists(CustomerServerInfo info) {
        List<CustomerServerInfo> list = mongoTemplate.find(Query.query(Criteria.where("openid").is(info.getOpenid())), CustomerServerInfo.class);
        if(list.isEmpty()){
            return false;
        }else{
            return true;
        }
    }

    public List<CustomerServerInfo> findAllServer(){

        List<CustomerServerInfo> all = mongoTemplate.findAll(CustomerServerInfo.class);
        return all;
    }

    public String delete(String openId){
        DeleteResult openid = mongoTemplate.remove(Query.query(Criteria.where("openid").is(openId)), CustomerServerInfo.class);
        return "success";
    }

    public CustomerServerInfo getServerInfo(String openId){
        return mongoTemplate.findOne(Query.query(Criteria.where("openid").is(openId)), CustomerServerInfo.class);
    }
}
