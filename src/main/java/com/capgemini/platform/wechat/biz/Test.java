package com.capgemini.platform.wechat.biz;

import javazoom.spi.mpeg.sampled.file.MpegAudioFileReader;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import java.io.File;

public class Test {

    public static void main(String[] args){
        File source= new File("D:\\abcd.wav");
        MpegAudioFileReader mp = new MpegAudioFileReader();
        try {
            AudioInputStream in = mp.getAudioInputStream(source);
            AudioFormat baseFormat = in.getFormat();
            AudioFormat targetFormat = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED, baseFormat.getSampleRate(), 16, baseFormat.getChannels(), baseFormat.getFrameSize(), baseFormat.getFrameRate(), false);
            System.out.println(targetFormat);
        }catch (Exception e){

        }

    }


}
