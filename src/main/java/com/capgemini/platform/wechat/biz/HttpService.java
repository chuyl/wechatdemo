package com.capgemini.platform.wechat.biz;

import com.capgemini.platform.wechat.domain.AuthorizationInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author chuyl
 * @date 11/30/2018
 */
@Component
public class HttpService {

    @Autowired
    MongoTemplate mongoTemplate;

    private RestTemplate restTemplate = new RestTemplate();

    private static final String sendUrl="https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token={access_token}";

    /**
     * @param token 微信token
     * @param content 要发送的内容
     * @param toUser user的 openId
     * @author chuyl
     * @date 11/30/2018
     */
    public String sendTextMsg(String token,String content,String toUser){
        Map<String, Object> map = new ConcurrentHashMap<>();
        Map<String, Object> contentMap = new HashMap<>();
        contentMap.put("content", content);
        map.put("touser", toUser);
        map.put("msgtype", "text");
        map.put("text", contentMap);
        return sendMsgPost(map,token);
    }

    /**
     * @param token 微信appId
     * @param mediaId 要发送的内容
     * @param toUser user的 openId
     * @author chuyl
     * @date 11/30/2018
     */
    public String sendImageMsg(String token,String mediaId,String toUser){
        Map<String, Object> map = new ConcurrentHashMap<>();
        Map<String, Object> contentMap = new HashMap<>();
        contentMap.put("media_id", mediaId);
        map.put("touser", toUser);
        map.put("msgtype", "image");
        map.put("image", contentMap);
        return sendMsgPost(map,token);
    }

    /**
     * @param token 微信appId
     * @param mediaId 要发送的内容
     * @param toUser user的 openId
     * @author chuyl
     * @date 11/30/2018
     */
    public String sendVoiceMsg(String token,String mediaId,String toUser){
        Map<String, Object> map = new ConcurrentHashMap<>();
        Map<String, Object> contentMap = new HashMap<>();
        contentMap.put("media_id", mediaId);
        map.put("touser", toUser);
        map.put("msgtype", "voice");
        map.put("voice", contentMap);
        return sendMsgPost(map,token);
    }

    /**
     * @param token 微信token
     * @param map 要发送的内容
     * @author chuyl
     * @date 11/30/2018
     */
    private String sendMsgPost(Map<String, Object> map, String token){
        restTemplate.getMessageConverters().set(1,new StringHttpMessageConverter(StandardCharsets.UTF_8));
        return restTemplate.postForObject(sendUrl, map, String.class,token);
    }

    public String getToken(String appId){
        AuthorizationInfo authorizationInfo = mongoTemplate.findOne(Query.query(Criteria.where("_id").is(appId)), AuthorizationInfo.class);
        return authorizationInfo.getAccessToken();
    }

}
