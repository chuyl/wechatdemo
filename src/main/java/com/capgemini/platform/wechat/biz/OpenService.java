package com.capgemini.platform.wechat.biz;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.capgemini.platform.chatbot.model.Chat;
import com.capgemini.platform.chatbot.service.AiuiService;
import com.capgemini.platform.chatbot.service.ChatService;
import com.capgemini.platform.chatbot.transport.AiuiRequest;
import com.capgemini.platform.chatbot.transport.AiuiResponse;
import com.capgemini.platform.config.properties.AppProperties;
import com.capgemini.platform.wechat.aes.AesException;
import com.capgemini.platform.wechat.domain.AuthorizationInfo;
import com.capgemini.platform.wechat.domain.Authorizer;
import com.capgemini.platform.wechat.domain.ComponentVerifyTicket;
import com.capgemini.platform.wechat.po.ComponentAccessToken;
import com.capgemini.platform.wechat.po.CustomerServerInfo;
import com.capgemini.platform.wechat.utils.Constants;
import com.capgemini.platform.wechat.utils.ConvertAudio;
import com.capgemini.platform.wechat.utils.OpenUtils;
import com.capgemini.platform.wechat.vo.TextMessage;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.dom4j.DocumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.stream.Collectors;

/**
 * @author: yyb
 * @date: 17-9-11
 */
@Service
public class OpenService {

    private static Logger logger = LoggerFactory.getLogger(OpenService.class);

    private volatile ComponentAccessToken componentAccessToken;

    private volatile ComponentVerifyTicket componentVerifyTicket;

    private RestTemplate restTemplate = new RestTemplate();

    public static Map<String, Chat> chatMap= new ConcurrentHashMap<>();

    public static Map<String, String> employMap= new ConcurrentHashMap<>();//key为太保测试openid，

    public static Map<String,String> kfMap =new ConcurrentHashMap<>();//客服map，key为客服openid,value为粉丝openid

    public static Map<String,String> fsMap =new ConcurrentHashMap<>();//粉丝map，key为粉丝openid,value为客服openid

    private static volatile Set<String> onkfSet = new HashSet<>();//人工咨询中的粉丝

    private static volatile Map<String,Map<String,Object>> taskMap=new ConcurrentHashMap<>(); //key为 客服map,value为 appid 和时间键值对

    public static volatile LinkedBlockingQueue<String> queue = new LinkedBlockingQueue<>(300);

    public static volatile Map<String, ConcurrentLinkedQueue<String>> msgCacheMap= new ConcurrentHashMap<>(); //key为粉丝的openid,value为缓存的消息，
    @Autowired
    private ChatService chatService;
    @Autowired
    MongoTemplate mongoTemplate;
    @Autowired
    private CustomerService cService;
    @Autowired
    private HttpService httpService;
    @Autowired
    private AppProperties appProperties;
    @Autowired
    private AiuiService aiuiService;

    public static Set<String> getOnkfSet() {
        return onkfSet;
    }

    public static void setOnkfSet(Set<String> onkfSet) {
        OpenService.onkfSet = onkfSet;
    }

    public static Map<String, Map<String, Object>> getTaskMap() {
        return taskMap;
    }

    public static void setTaskMap(Map<String, Map<String, Object>> taskMap) {
        OpenService.taskMap = taskMap;
    }

    /**
     * 获取预授权码： pre_auth_code
     * 请求地址：https://api.weixin.qq.com/cgi-bin/component/api_create_preauthcode?component_access_token=xxx
     * 请求参数：{"component_appid": "xxx"}
     * 请求结果：{"pre_auth_code:"xxx", expires_in: 600}
     *
     * @return
     */
    public String getPreAuthCode() {
        String url = "https://api.weixin.qq.com/cgi-bin/component/api_create_preauthcode?component_access_token={component_access_token}";
        Map<String, String> postData = Collections.singletonMap("component_appid", Constants.APP_ID);
        String result = restTemplate.postForObject(url, postData, String.class, getComponentAccessToken());
        JSONObject object = JSON.parseObject(result);
        return object.getString("pre_auth_code");
    }

    private String getComponentVerifyTicket() {
        if (componentVerifyTicket == null || StringUtils.isEmpty(componentVerifyTicket.getVerifyTicket())) {
            this.componentVerifyTicket = mongoTemplate.findOne(Query.query(Criteria.where("name").is("COMPONENT_VERIFY_TICKET")), ComponentVerifyTicket.class);
        }

        if (this.componentVerifyTicket != null) {
            return this.componentVerifyTicket.getVerifyTicket();
        }
        return null;
    }


    /**
     * 更新ticket
     *
     * @param componentVerifyTicket
     */
    private void updateComponentVerifyTicket(String componentVerifyTicket) {
        if (StringUtils.isEmpty(componentVerifyTicket)) {
            return;
        }
        ComponentVerifyTicket ticket = mongoTemplate.findOne(Query.query(Criteria.where("name").is("COMPONENT_VERIFY_TICKET")), ComponentVerifyTicket.class);
        if (ticket == null) {
            ticket = new ComponentVerifyTicket();
            ticket.setName("COMPONENT_VERIFY_TICKET");
        }
        ticket.setUpdateTime(System.currentTimeMillis());
        ticket.setVerifyTicket(componentVerifyTicket);
        mongoTemplate.save(ticket);
        this.componentVerifyTicket = ticket;
    }


    /**
     * 获取第三方平台的令牌
     *
     * @return
     */
    public String getComponentAccessToken() {
        if (StringUtils.isEmpty(getComponentVerifyTicket())) {
            return null;
        }

        if (componentAccessToken == null
                || System.currentTimeMillis() - componentAccessToken.getCreateTime() >= componentAccessToken.getExpiresIn() - 20 * 60 * 1000) {

            String url = "https://api.weixin.qq.com/cgi-bin/component/api_component_token";

            Map<String, String> postData = new HashMap<>();
            postData.put("component_appid", Constants.APP_ID);
            postData.put("component_appsecret", Constants.APP_SECRET);
            postData.put("component_verify_ticket", getComponentVerifyTicket());

            String result = restTemplate.postForObject(url, postData, String.class);

//            System.out.println("refr失败的原因是"+result);
            JSONObject resObj = JSONObject.parseObject(result);
            String accessToken = resObj.getString("component_access_token");
            if (StringUtils.isNotEmpty(accessToken)) {
                if (componentAccessToken == null) {
                    componentAccessToken = new ComponentAccessToken();
                }

                componentAccessToken.setCreateTime(System.currentTimeMillis());
                componentAccessToken.setExpiresIn(resObj.getLongValue("expires_in") * 1000);
                componentAccessToken.setComponentAccessToken(accessToken);
            } else {
                return null;
            }
        }
        return componentAccessToken.getComponentAccessToken();
    }

    /**
     * 保存公众号授权令牌
     *
     * @param authCode
     * @return
     */
    public AuthorizationInfo saveAuth(String authCode) {
        String url = "https://api.weixin.qq.com/cgi-bin/component/api_query_auth?component_access_token=" + getComponentAccessToken();
        Map<String, String> params = new HashMap<>();
        params.put("component_appid", Constants.APP_ID);
        params.put("authorization_code", authCode);

        // 返回公众号的调用凭据
        String result = restTemplate.postForObject(url, params, String.class);
        JSONObject resObj = JSON.parseObject(result);
        if (!resObj.containsKey("errcode")) {
            JSONObject infoObj = resObj.getJSONObject("authorization_info");
            String appId = infoObj.getString("authorizer_appid");

            AuthorizationInfo authorizationInfo = mongoTemplate.findOne(Query.query(Criteria.where("_id").is(appId)), AuthorizationInfo.class);
            if(authorizationInfo==null){
                authorizationInfo= new AuthorizationInfo();
            }
            authorizationInfo.setAppId(appId);
            authorizationInfo.setAccessToken(infoObj.getString("authorizer_access_token"));
            authorizationInfo.setRefreshToken(infoObj.getString("authorizer_refresh_token"));
            authorizationInfo.setExpiresIn(infoObj.getLongValue("expires_in") * 1000);
            authorizationInfo.setFuncInfo(infoObj.getJSONArray("func_info").toJavaObject(List.class));
            authorizationInfo.setCreateTime(System.currentTimeMillis());
            authorizationInfo.setUpdateTime(System.currentTimeMillis());

            mongoTemplate.save(authorizationInfo);

            return authorizationInfo;
        }
        return null;
    }

    /**
     * 保存公众号基本信息
     *
     * @param authorizationInfo
     * @return
     */
    public Authorizer getAuthorizer(AuthorizationInfo authorizationInfo) {
        String url = "https://api.weixin.qq.com/cgi-bin/component/api_get_authorizer_info?component_access_token={component_access_token}";

        Map<String, String> params = new HashMap<>();
        params.put("component_appid", Constants.APP_ID);
        params.put("authorizer_appid", authorizationInfo.getAppId());

        String result = restTemplate.postForObject(url, params, String.class, getComponentAccessToken());
        result = new String(result.getBytes(Charset.forName("ISO-8859-1")), Charset.forName("UTF-8"));
        if (result.contains("authorizer_info")) {

            Authorizer authorizer = mongoTemplate.findOne(Query.query(Criteria.where("appId").is(authorizationInfo.getAppId())), Authorizer.class);
            if (authorizer == null) {
                authorizer = new Authorizer();
            }
            authorizer.setAppId(authorizationInfo.getAppId());
            JSONObject object = JSONObject.parseObject(result);
            JSONObject info = object.getJSONObject("authorizer_info");
            authorizer.setNickname(info.getString("nick_name"));
            authorizer.setHeadImg(info.getString("head_img"));

            JSONObject service = info.getJSONObject("service_type_info");
            authorizer.setServiceTypeInfo(service.getInteger("id"));

            JSONObject verify = info.getJSONObject("verify_type_info");
            authorizer.setVerifyTypeInfo(verify.getInteger("id"));

            authorizer.setUsername(info.getString("user_name"));
            authorizer.setPrincipalName(info.getString("principal_name"));
            authorizer.setBusinessInfo(info.getJSONObject("business_info").toJavaObject(Map.class));

            authorizer.setAlias(info.getString(info.getString("alias")));
            authorizer.setQrcodeUrl(info.getString("qrcode_url"));

            mongoTemplate.save(authorizer);
            return authorizer;
        }
        return null;
    }

    /**
     * 发送消息
     *
     * @param response
     * @param content  要发送的消息
     * @throws IOException
     */
    public void output(HttpServletResponse response, String content) throws IOException {
        PrintWriter printWriter = response.getWriter();
        printWriter.print(content);
        printWriter.flush();
        printWriter.close();
    }


    /**
     * 处理授权事件URL
     *
     * @param request
     * @throws IOException
     * @throws AesException
     * @throws DocumentException
     */
    public void doEventAuthorize(HttpServletRequest request) throws IOException, AesException, DocumentException {
        String token = Constants.TOKEN;
        String nonce = request.getParameter("nonce");
        String timestamp = request.getParameter("timestamp");
        String signature = request.getParameter("signature");
        String msgSignature = request.getParameter("msg_signature");

        if (StringUtils.isEmpty(msgSignature)) {
            return;
        }
        // 微信消息签名验证
        boolean isValid = OpenUtils.checkSignature(token, signature, timestamp, nonce);
        if (!isValid) {
            return;
        }
        String xml = IOUtils.toString(request.getReader());
        Map<String, String> map = OpenUtils.decryptMsgToMap(msgSignature, timestamp, nonce, xml);
        logger.info("--->>> doEventAuthorize: {}", map.toString());
        String infoType = map.get("InfoType");
        switch (infoType) {
            case "component_verify_ticket":
                // TODO 推送ticket,这ticket也要保存起来，并且随着微信的推送刷新
                String componentVerifyTicket = map.get("ComponentVerifyTicket");
                this.updateComponentVerifyTicket(componentVerifyTicket);
                break;
            case "unauthorized":
                this.unauthorized(map);
                break;
            case "authorized":
                break;
            case "updateauthorized":
                this.createOrUpdateAuthorized(map);
                break;
        }
    }


    /**
     * 处理取消授权通知
     *
     * @param map：包含 AppId，CreateTime，AuthorizerAppid
     */
    private void unauthorized(Map<String, String> map) {
        String authoirzerAppId = map.get("AuthorizerAppid");
        mongoTemplate.remove(Query.query(Criteria.where("id").is(authoirzerAppId)), AuthorizationInfo.class);
        mongoTemplate.remove(Query.query(Criteria.where("id").is(authoirzerAppId)), Authorizer.class);
    }

    /**
     * 处理授权成功或授权更新通知
     *
     * @param map 包含：AppId，CreateTime，AuthorizerAppid，AuthorizationCode，AuthorizationCodeExpiredTime
     */
    private void createOrUpdateAuthorized(Map<String, String> map) {
        String authorizationCode = map.get("AuthorizationCode");
        AuthorizationInfo authorizationInfo = saveAuth(authorizationCode);
        getAuthorizer(authorizationInfo);
    }

    /**
     * 处理公众号的消息和事件
     *
     * @param request
     * @param response
     * @throws IOException
     * @throws AesException
     * @throws DocumentException
     */
    public void processMessageAndEvent(String appId, HttpServletRequest request, HttpServletResponse response) throws IOException, AesException, DocumentException {
        String token = Constants.TOKEN;
        System.out.println("开始接受消息啦");
        String nonce = request.getParameter("nonce");
        String timestamp = request.getParameter("timestamp");
        String signature = request.getParameter("signature");
        String msgSignature = request.getParameter("msg_signature");
        if (StringUtils.isBlank(msgSignature)) {
            // 微信推送给第三方开放平台的消息一定是加过密的，无消息加密无法解密消息
            return;
        }
        String xml = IOUtils.toString(request.getReader());
        boolean isValid = OpenUtils.checkSignature(token, signature, timestamp, nonce);
        if (isValid) {
            Map<String, String> map = OpenUtils.decryptMsgToMap(msgSignature, timestamp, nonce, xml);
            logger.info("--->>> processMessageAndEvent: {}", map.toString());

            String toUserName = map.get("ToUserName");

            // 微信全网测试: gh_3c884a361561 微信专用测试公众号；  gh_8dad206e9538 微信专用测试小程序
            if (StringUtils.equals(toUserName, Constants.MP_USERNAME) || StringUtils.equals(toUserName, Constants.MINI_USERNAME)) {
                // 微信全网发布检测的
                this.wxAllNetworkCheck(map, response);
            } else {
                // 公众号消息事件处理
                this.mpMessageAndEvent(appId, map, response);
            }
        }
    }

    /**
     * 微信全网发布检测
     */
    private void wxAllNetworkCheck(Map<String, String> map, HttpServletResponse response) throws IOException, AesException {
        String msgType = map.get("MsgType");
        String  toUserName= map.get("ToUserName");
        String fromUserName = map.get("FromUserName");//key
        if ("event".equalsIgnoreCase(msgType)) {
            // 全网检测的模拟粉丝点击事件，只要返回文本消息：事件名称+"from_callback"
            String event = map.get("Event");
            replyTextMessage(response, OpenUtils.generateTextXML(fromUserName, toUserName, event + "from_callback"));

        } else if ("text".equalsIgnoreCase(msgType)) {
            String content = map.get("Content");
            if ("TESTCOMPONENT_MSG_TYPE_TEXT".equalsIgnoreCase(content)) {
                //全网检测的模拟模板消息，只要返回文本消息： 内容 + "_callback"
                replyTextMessage(response, OpenUtils.generateTextXML(fromUserName, toUserName, content + "_callback"));
            } else if (StringUtils.startsWithIgnoreCase(content, "QUERY_AUTH_CODE")) {
                // 模拟粉丝发送文本消息给专用测试公众号，第三方平台方需在5秒内返回空串表明暂时不回复，然后再立即使用客服消息接口发送消息回复粉丝
                this.output(response, "");
                String msg = content.split(":")[1];
                // 调用客服接口回复消息
                this.replyApiTextMessage(msg, fromUserName);
            }
        }
    }


    /**
     * 公众号处理事件
     * @author chuyl
     * @date 11/30/2018
     */
    private void mpMessageAndEvent(String appId, Map<String, String> map, HttpServletResponse response) throws IOException, AesException {
        String msgType = map.get("MsgType");
        System.out.println("msgtyoe:"+msgType);
        System.out.println("map:"+map);
        String toUserName = map.get("ToUserName");
        String fromUserName = map.get("FromUserName");
        List<CustomerServerInfo> allServer = cService.findAllServer();
        System.out.println("allserver:"+allServer);
        List<String> strCs = allServer.stream().map(a -> a.getOpenid()).collect(Collectors.toList());//所有的客服
        System.out.println("strCs:"+strCs);
        switch (msgType) {
            case "text":
                receiveTextMessage(appId, map, response);
                return;
            case "image":
                this.output(response, "");
                if(strCs.contains(fromUserName)&&kfMap.containsKey(fromUserName)&&kfMap.get(fromUserName)!=null){//客服回复消息走字段逻辑
                    this.kfAnswerImage(fromUserName,map,"B",appId);
                }else{
                    if(getOnkfSet().contains(fromUserName)){//咨询中
                        this.kfAnswerImage(fromUserName,map,"A",appId);
                    }
                }
                return;
                // 图片消息
            case "voice":
                this.handleVoiceMessage(appId,map,response,strCs);
                break;
                // 音频消息
            case "video":
                break;
                // 视频消息
            case "shortvideo":
                break;
                // 小视频消息
            case "location":
                break;
                // 地理位置消息
            case "link":
                break;
                // 链接消息
            case "event":
                // 事件
                String event = map.get("Event").toLowerCase();
                switch (event) {
                    case "subscribe":
                        break;
                        // 关注事件
                    case "unsubscribe":
                        break;
                        // 取关事件
                    case "scan":
                        break;
                        // 扫描事件
                    case "location":
                        break;
                        // 上报地理位置事件
                    case "click":
                        break;
                        // 点击菜单拉取消息时的事件
                    case "view":
                        break;
                        // 点击菜单跳转链接时的事件
                }
                break;
             default:
        }

        this.output(response, "");
    }

    /*
     *处理语音消息
     */
    private void handleVoiceMessage(String appId, Map<String, String> map, HttpServletResponse response,List<String> listKf) throws IOException, AesException {

        String token= httpService.getToken(appId);
        System.out.println("token:"+token);
        String mediaId= map.get("MediaId");
        System.out.println("mediaId:"+mediaId);
        String user=map.get("FromUserName");

        if(listKf.contains(user)){//客服人员
            if(kfMap.containsKey(user)){
                String fsId= kfMap.get(user);
                if(fsId!=null){//说明证在服务中，直接转发就OK
                    fullTaskMap(appId, user);
                    httpService.sendVoiceMsg(token,mediaId,fsId);
                }
            }
        }else{//粉丝
            if(getOnkfSet().contains(user)){//咨询中
                this.output(response, "");
                if(fsMap!=null&&fsMap.containsKey(user)){
                    fullTaskMap(appId, fsMap.get(user));
                    String kfId= fsMap.get(user);
                    httpService.sendVoiceMsg(token,mediaId,kfId);
                }
            }else{
                String url = "http://file.api.weixin.qq.com/cgi-bin/media/get?access_token=ACCESS_TOKEN&media_id=MEDIA_ID";
                url = url.replace("ACCESS_TOKEN", token).replace("MEDIA_ID", mediaId);
                String msg =getAudioAnswer(url,user);
                map.put("Content",msg);
                System.out.println("语音放返回的问题是"+msg);
                receiveTextMessage(appId, map, response);
            }
        }

    }

    private String getAudioAnswer(String url,String user) {
        String msg="";
        try {
            URL url2 = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) url2.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.connect();

            InputStream is = conn.getInputStream();
            // 根据内容类型获取扩展名
            // 将mediaId作为文件名
            String path =appProperties.getUploadPath();
            UUID uuamr = UUID.randomUUID();
            UUID uuwav = UUID.randomUUID();
            String amrName= uuamr.toString()+".amr";
            String wavName= uuwav.toString()+".wav";
            File source= new File(path,amrName);
            File target =new File(path,wavName);
            BufferedInputStream bis = new BufferedInputStream(conn.getInputStream());

            FileOutputStream fos = new FileOutputStream(source);
//            bis.available()
            System.out.println("长度:"+bis.available());
            byte[] buf = new byte[4096];
            int size = 0;
            while ((size = bis.read(buf)) != -1) {
                fos.write(buf, 0, size);
            }
            fos.close();
            bis.close();
            conn.disconnect();
            ConvertAudio.Amr2Wav(source,target);
            source.delete();
            msg =getChatMessageByAudio(user,target.getAbsolutePath());
        } catch (Exception e) {
            logger.info(e.toString());
        }
        return msg;
    }

    private String getChatMessageByAudio(String key,String file){
        Chat chat =null;
        if(chatMap.containsKey(key)){
            chat = chatMap.get(key);
        }else{
            chat=new Chat();
            chat.setPid(1L);
            chat.setDataType(1);
            chat.setScene("main");
            chatMap.put(key,chat);
        }

        AiuiResponse response = new AiuiResponse();
        try {

            response = speechRecognition(file,chat);
            chat.setQuestion(response.getAsk());
        } catch (Exception e) {
        }
        return chat.getQuestion();
    }

    private AiuiResponse speechRecognition(String wavFile,Chat chat) throws Exception {

        //讯飞语音识别接口识别wav音频文件，转成文字返回
        AiuiRequest request = new AiuiRequest();
        request.setDataType("audio");
        request.setUserCode("admin");
        request.setScene(chat.getScene()!=null?chat.getScene():"main");
        request.setFileName(wavFile);
        AiuiResponse response = aiuiService.searchAnswer(request);
        return response;
    }

    private void receiveTextMessage(String appId, Map<String, String> map, HttpServletResponse response) throws IOException, AesException {

        String toUserName = map.get("ToUserName");
        String fromUserName = map.get("FromUserName");
        List<CustomerServerInfo> allServer = cService.findAllServer();
        List<String> strCs = allServer.stream().map(a -> a.getOpenid()).collect(Collectors.toList());//所有的客服
        TextMessage textMessage = new TextMessage();
        this.output(response, "");
//        replyTextMessage( response, OpenUtils.generateTextXML(fromUserName, toUserName, "_callback"));
        if("我的openid是多少".equals(map.get("Content"))){
            httpService.sendTextMsg(httpService.getToken(appId),fromUserName,fromUserName);
            return;
        }
        if(strCs.contains(fromUserName)){//客服回复消息走字段逻辑
            if(kfMap.get(fromUserName)==null||kfMap.get(fromUserName).isEmpty()){//未接入状态
                if("接入".equals(map.get("Content"))){
                    String fs=null;
                    if(queue.size()>0){
                        fs=queue.poll();
                    }
                    this.jieru(appId,fs,fromUserName);
                }
            }else{
                if ("断开".equals(map.get("Content"))){
                    this.duankai(appId,fromUserName);
                }else{
                    this.kfAnswerMsg(fromUserName,map.get("Content"),"B",appId);
                }
            }
        }else{
            if(getOnkfSet().contains(fromUserName)){//咨询中
                this.kfAnswerMsg(fromUserName,map.get("Content"),"A",appId);
            }else{
                String conte=map.get("Content");
                if("结束人工客服".equals(conte)){
                    this.rengongduankai(appId,fromUserName);
                    return;
                }

                if(msgCacheMap.containsKey(fromUserName)){
                    cacheFsMsg(fromUserName,conte);
                }else{
                    if("人工客服".equals(conte)||"人工".equals(conte)||"转人工客服".equals(conte)||
                            "转人工".equals(conte)){
                        String msg = "";
                        // 调用客服接口回复消息
                        this.customerMsg(appId,msg, fromUserName);
                    }else{
                        String contdd =map.get("Content");
                        System.out.println("contdd:"+contdd);
                        httpService.sendTextMsg(httpService.getToken(appId),getAnswerFromTaibao(fromUserName,contdd),fromUserName);
                    }
                }
            }
        }
    }

    private String getAnswerFromTaibao(String fromUserName,String contdd){
        String taibaoUrl = "http://ts.doramn.com:8500/PSIGW/RESTListeningConnector/PSFT_HR/PCMReport.v1/?AccessToken=70b31eae-ec79-11e8-99f5-a9d05de634b5&SourceRecord=CG_FCSTBAL_VW&param1={employId}&param2=2018";
        if(employMap.containsKey(fromUserName)){
            employMap.remove(fromUserName);
            String employId= contdd;
//            String url= "http://localhost:8099/getYear";
            try {
                Object forObject = restTemplate.getForObject(taibaoUrl, Object.class,employId);
                LinkedHashMap data =  (LinkedHashMap) ((ArrayList) ((LinkedHashMap) ((ArrayList) forObject).get(0)).get("data")).get(0);

                String msg= "您今年共有"+data.get("CG_ANN_LEV_ENT")+"天年假，已使用"+data.get("CG_ANN_LEV_TAKE")+"天，剩余"+data.get("CG_ANN_LEV_BAL")+"天可用。";
                return msg;
            }catch (Exception e){
                return "找不到您的年假信息,请检查员工号";
            }

        }else{
            String chatbotanswer =getAnswerFromChatBot(fromUserName,contdd);
            if("answernianjia123".equals(chatbotanswer)){
                employMap.put(fromUserName,fromUserName);
                return "请输入您要查询的员工id";
            }else{
                return chatbotanswer;
            }
        }
    }

    private void rengongduankai(String appId, String fromUserName) {
        String token= httpService.getToken(appId);
        getOnkfSet().remove(fromUserName);
        if(fsMap.containsKey(fromUserName)){
            String kfId= fsMap.get(fromUserName);
            if(kfId!=null){
                kfMap.remove(kfId);
                getTaskMap().remove(kfId);
            }
        }
        msgCacheMap.remove(fromUserName);
        fsMap.remove(fromUserName);
        httpService.sendTextMsg(token,"人工客服已断开，Chatbot继续为您服务",fromUserName);

        sendToFreeKf(token);
    }

    private void cacheFsMsg(String fromUserName,String content) {
        ConcurrentLinkedQueue<String> qq = msgCacheMap.get(fromUserName);
        if(qq.size()<20){
            qq.add(content);
        }
    }

    /**
     * @author chuyl
     * @date 11/30/2018
     */
    private void kfAnswerImage(String fromUserName, Map<String, String> map, String type, String appId) {
        String mediaId= map.get("MediaId");
        String token =httpService.getToken(appId);
        String toUserName="";
        if("A".equals(type)){
            toUserName = fsMap.get(fromUserName);//客服
        }else{
            toUserName = kfMap.get(fromUserName);//粉丝
        }

        String kfres = httpService.sendImageMsg(token,mediaId,toUserName);
        logger.info("-->> 转发image的消息: {}", kfres);
    }

    private void duankai(String appId, String kfId) {
        String token= httpService.getToken(appId);

        if(kfMap.containsKey(kfId)){
            String s = kfMap.get(kfId);
            if(s!=null){
                getOnkfSet().remove(s);
                fsMap.remove(s);
            }
            kfMap.remove(kfId);
            getTaskMap().remove(kfId);
            httpService.sendTextMsg(token,"人工客服已断开，Chatbot继续为您服务",s);
        }
        sendToFreeKf(token);
    }

    private void jieru(String appId, String fsId, String kfId) {
        String token = httpService.getToken(appId);
        System.out.println("客服id是"+kfId+"粉丝id是"+fsId);
        if(fsId!=null){
            kfMap.put(kfId,fsId);
            getOnkfSet().add(fsId);
            fsMap.put(fsId,kfId);
            fullTaskMap(appId, kfId);
            String fftpd=httpService.sendTextMsg(token,"已为您接入该用户",kfId);
            if(msgCacheMap.containsKey(fsId)){
                ConcurrentLinkedQueue<String> unReadMsgs = msgCacheMap.get(fsId);
                while (unReadMsgs.size()>0){
                    String unReadMsg= unReadMsgs.poll();
                    httpService.sendTextMsg(token,unReadMsg,kfId);
                }
                msgCacheMap.remove(fsId);
            }
            System.out.println("kfid:"+kfId);
            CustomerServerInfo serverInfo = cService.getServerInfo(kfId);
            String content="您好，现在是客服";
            if(serverInfo!=null&&serverInfo.getName()!=null){
                content+=serverInfo.getName();
            }
            System.out.println("serverInfo.getName():"+serverInfo.getName());
            content+="为您服务，请问您有什么问题？";
            String ffsdddd=httpService.sendTextMsg(token,content,fsId);
            System.out.println("ffsdddd"+ffsdddd);
            this.sendToFreeKf(token);
        }else{
            httpService.sendTextMsg(token,"没有在咨询的粉丝",kfId);
        }
    }

    private void fullTaskMap(String appId, String kfId) {
        Map<String,Object> map1 =new ConcurrentHashMap<>();
        map1.put("time",System.currentTimeMillis());
        map1.put("appId",appId);
        getTaskMap().put(kfId,map1);
    }

    private void kfAnswerMsg(String fromUserName,String content,String type,String appId) {
        String toUserName=null;
        if("A".equals(type)){
            if(fsMap!=null&&fsMap.containsKey(fromUserName)){
                fullTaskMap(appId, fsMap.get(fromUserName));
                toUserName= fsMap.get(fromUserName);
            }
        }else{
            toUserName = kfMap.get(fromUserName);//粉丝
            fullTaskMap(appId, fromUserName);
        }
        System.out.println("fullTaskMap:" + getTaskMap());
        System.out.println("appid："+appId+"   touser:"+toUserName);
        if(toUserName!=null){
            httpService.sendTextMsg(httpService.getToken(appId),content,toUserName);
        }

    }

    /**
     * 发挥消息文本
     *
     * @param response
     * @param message
     * @throws IOException
     */
    private void replyTextMessage(HttpServletResponse response, String message) throws IOException, AesException {
        // 加密要发送的消息
        System.out.println("final message:"+message);
        String encryptXml = OpenUtils.encryptMsg(message);
        this.output(response, encryptXml);
    }

    /**
     * 第三方平台方拿到$query_auth_code$的值后，
     * 通过接口文档页中的“使用授权码换取公众号的授权信息”API，
     * 将$query_auth_code$的值赋值给API所需的参数authorization_code。
     * 然后，调用发送客服消息api回复文本消息给粉丝，
     * 其中文本消息的content字段设为：$query_auth_code$_from_api（其中$query_auth_code$需要替换成推送过来的query_auth_code）
     *
     * @param authCode
     * @param toUserName
     */
    private void replyApiTextMessage(String authCode, String toUserName) {
        // 先通过 authCode 获取到公众号的 accessToken
        String accessTokenUrl = "https://api.weixin.qq.com/cgi-bin/component/api_query_auth?component_access_token={component_access_token}";
        Map<String, String> params = new HashMap<>();
        params.put("component_appid", Constants.APP_ID);
        params.put("authorization_code", authCode);
        String accessTokenResult = restTemplate.postForObject(accessTokenUrl, params, String.class, getComponentAccessToken());
        logger.info("-->> 全网发布api检测: {}", accessTokenResult);
        JSONObject object = JSONObject.parseObject(accessTokenResult);

        JSONObject authorizationInfo = object.getJSONObject("authorization_info");

        // 通过公众号的accessToken调用客服接口发送消息
        Map<String, Object> obj = new HashMap<String, Object>();
        Map<String, Object> msgMap = new HashMap<String, Object>();
        String msg = authCode + "_from_api";
        msgMap.put("content", msg);
        obj.put("touser", toUserName);
        obj.put("msgtype", "text");
        obj.put("text", msgMap);
        String sendUrl = "https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token={access_token}";
        String sendResult = restTemplate.postForObject(sendUrl, obj, String.class, authorizationInfo.getString("authorizer_access_token"));
        logger.info("-->> 全网检测发送消息: {}", sendResult);
    }

    /**
     * 客服接口
     * @author chuyl
     * @date 11/26/2018
     */
    private void customerMsg(String appId, String msg, String toUserName) {
        String token = httpService.getToken(appId);

        try {
            System.out.println("put haole");
            queue.put(toUserName);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("appId:"+appId+";msg+"+"toUsrName:"+toUserName);
        if(token!=null){
            System.out.println("token"+token);

            List<CustomerServerInfo> allServer = cService.findAllServer();
            List<String> strCs = allServer.stream().map(a -> a.getOpenid()).collect(Collectors.toList());//所有的客服

            String content ="您好，正在为您转接人工客服，请问您有什么问题？";

            ConcurrentLinkedQueue<String> queue=new ConcurrentLinkedQueue<String>();
            msgCacheMap.put(toUserName,queue);
            String kfres = httpService.sendTextMsg(token, content, toUserName);
            logger.info("-->> 客服发送消息给客户: {}", kfres);
            sendToFreeKf(token);
        }
    }

    public void sendToFreeKf(String token){
        List<CustomerServerInfo> allServer = cService.findAllServer();
        List<String> strCs = allServer.stream().map(a -> a.getOpenid()).collect(Collectors.toList());//所有的客服
        Set<String> workKf = kfMap.keySet();//服务中的的客服
        List<String> freeKfs = strCs.stream().filter(a -> !workKf.contains(a)).collect(Collectors.toList());

        String content="当前等待人数为"+queue.size()+"人，回复\"接入\"进行接入，结束请回复\"断开\"";
        freeKfs.forEach(a->{
            String kfres = httpService.sendTextMsg(token,content,a);
            logger.info("-->> 给空闲客服发送消息: {}", kfres);
        });

    }

    public void refresh(AuthorizationInfo info) {
        String url = "https://api.weixin.qq.com/cgi-bin/component/api_authorizer_token?component_access_token={component_access_token}";

        Map<String, String> params = new HashMap<>();
        params.put("component_appid", Constants.APP_ID);
        params.put("authorizer_appid", info.getAppId());
        params.put("authorizer_refresh_token", info.getRefreshToken());

        String result = restTemplate.postForObject(url, params, String.class, this.getComponentAccessToken());
        if (result.contains("authorizer_access_token")) {
            JSONObject object = JSONObject.parseObject(result);
            String accessToken = object.getString("authorizer_access_token");
            Long expiresIn = object.getLong("expires_in");
            String newRefreshToken = object.getString("authorizer_refresh_token");

            Update update = new Update();
            update.set("accessToken", accessToken);
            update.set("expiresIn", expiresIn * 1000);
            update.set("refreshToken", newRefreshToken);
            update.set("updateTime", System.currentTimeMillis());
            mongoTemplate.updateFirst(Query.query(Criteria.where("appId").is(info.getAppId())), update, AuthorizationInfo.class);
        } else {
            logger.error("刷新{}令牌错误", info.getAppId());
        }
    }

    private String getAnswerFromChatBot(String key,String content){
        Chat chat =null;
        if(chatMap.containsKey(key)){
            chat = chatMap.get(key);
        }else{
            chat=new Chat();
            chat.setPid(1L);
            chat.setDataType(1);
            chat.setScene("main");
        }
        chat.setQuestion(content);
        
        System.out.println("pid:" +chat.getPid());
        System.out.println("question"+content);

        ResponseEntity<Chat> entity = null;
        try {
            entity = chatService.chat(chat);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Chat body = entity.getBody();
        System.out.println("laspid"+body.getPid());
        System.out.println("body:"+body);
        chatMap.put(key,body);
        return body.getAnswer();
    }

    public ResponseEntity getPsMessage(String conversionId, String question) {
        String answer= getAnswerFromTaibao(conversionId,question);
        ResponseEntity<String> po= new ResponseEntity(answer,HttpStatus.OK);
        return po;
    }
}
