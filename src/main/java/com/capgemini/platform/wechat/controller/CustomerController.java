package com.capgemini.platform.wechat.controller;

import com.capgemini.platform.wechat.po.PsRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.StandardCharsets;

/**
 *
 * @Author:yanlchu
 * @Date:1/25/2019
 */
@RestController
public class CustomerController {

    private RestTemplate restTemplate = new RestTemplate();
    @GetMapping("/getYear")
    public ResponseEntity<YearDetail> getYear() {
        YearDetail po=new YearDetail();
        po.setNianjia(12);
        po.setNianjiashiyong(3);
        po.setYear("2019");

        return new ResponseEntity(po, HttpStatus.OK);
    }

    @GetMapping("/getTest")
    public String getTest() {
        String url ="https://chatbot.cheerstek.com/wechat/getPSMessage";
        PsRequest po=new PsRequest();
        po.setConversionId("123");
        po.setQuestion("你好");
        restTemplate.getMessageConverters().set(1,new StringHttpMessageConverter(StandardCharsets.UTF_8));
        String o = restTemplate.postForObject(url, po, String.class);

        String u2rl ="https://chatbot.cheerstek.com/wechat/getPSMsg?conversionId={}&question=你好";
        String forObject = restTemplate.getForObject(u2rl, String.class, "123");
        return "o";
    }
}
