package com.capgemini.platform.wechat.controller;

import com.capgemini.platform.chatbot.model.Chat;
import com.capgemini.platform.chatbot.service.ChatService;
import com.capgemini.platform.wechat.aes.AesException;
import com.capgemini.platform.wechat.biz.CustomerService;
import com.capgemini.platform.wechat.biz.OpenService;
import com.capgemini.platform.wechat.domain.AuthorizationInfo;
import com.capgemini.platform.wechat.po.CustomerServerInfo;
import com.capgemini.platform.wechat.po.PsRequest;
import com.capgemini.platform.wechat.utils.Constants;
import org.dom4j.DocumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;

/**
 * @author: yyb
 * @date: 17-9-11
 */
@RestController
public class OpenController {

    @Autowired
    private ChatService chatService;

    private static Logger logger = LoggerFactory.getLogger(OpenController.class);

    @Autowired
    OpenService openService;
    @Autowired
    CustomerService cs;

    /**
     * 授权页面
     *
     * @return
     */
    @GetMapping("/open")
    public ModelAndView open() {
        ModelAndView view = new ModelAndView("open");
        String location = "https://mp.weixin.qq.com/cgi-bin/componentloginpage?"
                + "component_appid=" + Constants.APP_ID
                + "&pre_auth_code=" + openService.getPreAuthCode()
                + "&redirect_uri=https://" + Constants.HOST + "/wechat/open/auth";

        view.addObject("location", location);
        return view;
    }

    @GetMapping("/openp")
    public ModelAndView openp() {
        ModelAndView view = new ModelAndView("open");
        String location = "https://mp.weixin.qq.com/cgi-bin/componentloginpage?"
                + "component_appid=" + Constants.APP_ID
                + "&pre_auth_code=" + openService.getPreAuthCode()
                + "&redirect_uri=https://" + Constants.HOST + "/wechat/open/auth"
                +" &auth_type=2";
        view.addObject("location", location);
        return view;
    }

    @GetMapping("/open1/{content}")
    public String open1(@PathVariable String content)  {
        Chat chat =null;
        if(OpenService.chatMap.containsKey("113")){
            chat = OpenService.chatMap.get("113");
        }else{
            chat=new Chat();
            chat.setPid(1L);
            chat.setDataType(1);
            chat.setScene("main");
        }
        chat.setQuestion(content);

        System.out.println("pid:" +chat.getPid());
        System.out.println("question"+content);

        ResponseEntity<Chat> entity = null;
        try {
            entity = chatService.chat(chat);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Chat body = entity.getBody();
        OpenService.chatMap.put("123",body);
        return body.getAnswer();
        
    }

    @GetMapping("/addopen/{id}/{name}")
    public String open2(@PathVariable String id,@PathVariable String name)  {
        CustomerServerInfo info =new CustomerServerInfo();
        info.setOpenid(id);
        info.setName(name);
        return cs.addCustomerServer(info);
    }

    @GetMapping("/deleteopen/{id}")
    public String deleteOpen(@PathVariable String id)  {
        return cs.delete(id);
    }

    /**
     * 授权回调页面
     *
     * @param authCode
     * @param expiresIn
     * @return
     */
    @GetMapping("/open/auth")
    public ModelAndView openAuth(@RequestParam(name = "auth_code") String authCode,
                                 @RequestParam(name = "expires_in") Integer expiresIn) {
        ModelAndView view = new ModelAndView("success");
        AuthorizationInfo authorizationInfo = openService.saveAuth(authCode);
        openService.getAuthorizer(authorizationInfo);
        return view;
    }

    @PostMapping("/open/event/authorize")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void doEventAuthorize(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            openService.doEventAuthorize(request);
        } catch (Exception e) {
            logger.error("-->> doEventAuthorize error", e);
        } finally {
            openService.output(response, "success");
        }
    }

    @PostMapping("/open/{appId}/callback")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void callback(@PathVariable String appId, HttpServletRequest request, HttpServletResponse response) throws DocumentException, AesException, IOException {
        openService.processMessageAndEvent(appId, request, response);
    }

//    @PostMapping(value="/getPSMessage")
//    public ResponseEntity getMessage(@Valid @RequestBody PsRequest request){
//
//        if(request.getConversionId()==null||request.getQuestion()==null){
//            return new ResponseEntity(HttpStatus.BAD_REQUEST);
//        }
//        return openService.getPsMessage(request.getConversionId(),request.getQuestion());
//    }

    @GetMapping(value="/getPSMessage")
    public ResponseEntity getMessage1(@RequestParam(value = "conversionId")String conversionId,
                                      @RequestParam(value = "question")String question){

        if(conversionId==null||question==null){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        return openService.getPsMessage(conversionId,question);
    }
}
