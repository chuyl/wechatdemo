package com.capgemini.platform.wechat.controller;

import com.capgemini.platform.wechat.utils.CheckUtil;
import com.capgemini.platform.wechat.utils.MessageUtil;
import org.dom4j.DocumentException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

/**
 * @author chuyl
 * @date 11/7/2018
 */
@RestController
public class LoginController {

    @RequestMapping(value = "/wx",method= RequestMethod.GET)
    public void login(HttpServletRequest request, HttpServletResponse response){
        System.out.println("success");
        String signature = request.getParameter("signature");
        System.out.println("sign-"+signature);
        String timestamp = request.getParameter("timestamp");
        System.out.println("times+"+timestamp);
        String nonce = request.getParameter("nonce");
        System.out.println("nonce+"+nonce);
        String echostr = request.getParameter("echostr");
        System.out.println("echostr"+echostr);
        PrintWriter out = null;
        try {
            out = response.getWriter();
            System.out.println("out:"+out);
            if(CheckUtil.checkSignature(signature, timestamp, nonce)){
                out.write(echostr);
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }finally{
            out.close();
        }
    }

    @PostMapping(value = "/wx")
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, DocumentException {
        Map<String, String> map = MessageUtil.xmlToMap(request);
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        String toUserName = map.get("ToUserName");
        String fromUserName = map.get("FromUserName");
        String msgType = map.get("MsgType");
        String content = map.get("Content");

        PrintWriter writer = response.getWriter();
        String message="";
        if(MessageUtil.MESSAGE_TEXT.equals(msgType)){
            if("1".equals(content)){
                message=MessageUtil.initText(toUserName,fromUserName,MessageUtil.firstMenu());
            }else if("2".equals(content)){
                message=MessageUtil.initText(toUserName,fromUserName,MessageUtil.secondMenu());
            }else if("?".equals(content)||"？".equals(content)){
                message=MessageUtil.initText(toUserName,fromUserName,MessageUtil.menuText());
            }

        }else if(MessageUtil.MESSAGE_EVENT.equals(msgType)){
            String eventType= map.get("Event");
            if(MessageUtil.MESSAGE_SUBSCRIBE.equals(eventType)){
                message=MessageUtil.initText(toUserName,fromUserName,MessageUtil.menuText());
            }
        }
        System.out.println(message);
        writer.print(message);
        writer.close();
    }
}