package com.capgemini.platform.config;

import com.alibaba.druid.pool.DruidDataSource;
import com.capgemini.platform.config.properties.DruidProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import tk.mybatis.spring.annotation.MapperScan;

/**
 * MybatisPlus配置
 *
 * @author Gavin
 * @Date 2018/4/1 21:58
 */
@Configuration
/**
 * 如果引入多数据源，所以让spring事务的aop要在多数据源切换aop的后面
 */
@EnableTransactionManagement(order = 2)
@MapperScan(basePackages = {"com.capgemini.platform.chatbot.dao"})
public class MybatisPlusConfig {

    /**
     *
     */
    @Autowired
    DruidProperties druidProperties;


    /**
     * app的数据源
     */
    private DruidDataSource dataSource(){
        DruidDataSource dataSource = new DruidDataSource();
        druidProperties.config(dataSource);
        return dataSource;
    }

    /**
     * 单数据源连接池配置
     */
    @Bean
    public DruidDataSource singleDatasource() {
        return dataSource();
    }
}
