package com.capgemini.platform.config.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @Author:yanlchu
 * @Date:12/4/2018
 */
@Component
@ConfigurationProperties(prefix = AppProperties.PREFIX)
public class AppProperties {

    public static final String PREFIX = "app";

    private String uploadPath;

    public String getUploadPath() {
        return uploadPath;
    }

    public void setUploadPath(String uploadPath) {
        this.uploadPath = uploadPath;
    }
}
