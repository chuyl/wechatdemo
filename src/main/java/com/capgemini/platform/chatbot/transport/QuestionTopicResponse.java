package com.capgemini.platform.chatbot.transport;

import java.util.List;

public class QuestionTopicResponse extends BaseResponse{

    private String code;

    private String topic;

    private String answer;

    private Boolean whetherShow;

    private Long categoryId;

    private List<QuestionListResponse> questions;

    private List<AnswerListResponse> answers;

    /**
     * @return code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return topic
     */
    public String getTopic() {
        return topic;
    }

    /**
     * @param topic
     */
    public void setTopic(String topic) {
        this.topic = topic;
    }

    /**
     * @return category_id
     */
    public Long getCategoryId() {
        return categoryId;
    }

    /**
     * @param categoryId
     */
    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public List<QuestionListResponse> getQuestions() {
        return questions;
    }

    public void setQuestions(List<QuestionListResponse> questions) {
        this.questions = questions;
    }

    public List<AnswerListResponse> getAnswers() {
        return answers;
    }

    public void setAnswers(List<AnswerListResponse> answers) {
        this.answers = answers;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public Boolean getWhetherShow() {
        return whetherShow;
    }

    public void setWhetherShow(Boolean whetherShow) {
        this.whetherShow = whetherShow;
    }
}
