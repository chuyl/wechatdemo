package com.capgemini.platform.chatbot.transport;

/**
 * @author Gavin
 * @date 2018/5/22.
 */
public class AiuiResponse {
    private String code;
    private String data;
    private String desc;
    private String sid;
    private String ask;

    public AiuiResponse() {
    }

    public AiuiResponse(String code, String data, String desc, String sid, String ask) {
        this.code = code;
        this.data = data;
        this.desc = desc;
        this.sid = sid;
        this.ask = ask;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public String getAsk() {
        return ask;
    }

    public void setAsk(String ask) {
        this.ask = ask;
    }

    @Override
    public String toString() {
        return "AiuiResponse{" +
                "code='" + code + '\'' +
                ", data='" + data + '\'' +
                ", desc='" + desc + '\'' +
                ", sid='" + sid + '\'' +
                ", ask='" + ask + '\'' +
                '}';
    }
}
