package com.capgemini.platform.chatbot.transport;

import java.util.ArrayList;
import java.util.List;

public class QuestionTopicRequest extends BaseRequest{

    private String code;

    private String topic;

    private String answer;

    private Long categoryId;
    
    private String questionOrAnswer;

    private Boolean whetherShow=false;

    private List<QuestionListRequest> questions;

    private List<AnswerListRequest> answers;

    /**
     * @return code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return topic
     */
    public String getTopic() {
        return topic;
    }

    /**
     * @param topic
     */
    public void setTopic(String topic) {
        this.topic = topic;
    }

    /**
     * @return category_id
     */
    public Long getCategoryId() {
        return categoryId;
    }

    /**
     * @param categoryId
     */
    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }


    public List<QuestionListRequest> getQuestions() {
        if(questions==null){
            return new ArrayList<QuestionListRequest>();
        }
        return questions;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public void setQuestions(List<QuestionListRequest> questions) {
        this.questions = questions;
    }

    public List<AnswerListRequest> getAnswers() {
        if(answers==null){
            return new ArrayList<AnswerListRequest>();
        }
        return answers;
    }

    public void setAnswers(List<AnswerListRequest> answers) {
        this.answers = answers;
    }

    public String getQuestionOrAnswer() {
        return questionOrAnswer;
    }

    public void setQuestionOrAnswer(String questionOrAnswer) {
        this.questionOrAnswer = questionOrAnswer;
    }

    public Boolean getWhetherShow() {
        return whetherShow;
    }

    public void setWhetherShow(Boolean whetherShow) {
        this.whetherShow = whetherShow;
    }
}
