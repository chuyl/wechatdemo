package com.capgemini.platform.chatbot.transport;

public class UnansweredQuestionRequest extends BaseRequest{

    private Long categoryId;

    private String question;

    private String remark;

    private Boolean handleStatus;

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Boolean getHandleStatus() {
        return handleStatus;
    }

    public void setHandleStatus(Boolean handleStatus) {
        this.handleStatus = handleStatus;
    }
}
