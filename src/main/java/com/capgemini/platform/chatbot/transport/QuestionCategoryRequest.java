package com.capgemini.platform.chatbot.transport;

import java.util.List;

/**
 * @author chuyl
 * @date 10/19/2018
 */
public class QuestionCategoryRequest extends BaseRequest {

    private String code;

    private String categoryName;

    private Long parentId;

    private Integer orderNum;

    private String scene;

    private String path;

    private List<Long> toshows;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Integer getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(Integer orderNum) {
        this.orderNum = orderNum;
    }

    public String getScene() {
        return scene;
    }

    public void setScene(String scene) {
        this.scene = scene;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public List<Long> getToshows() {
        return toshows;
    }

    public void setToshows(List<Long> toshows) {
        this.toshows = toshows;
    }
}
