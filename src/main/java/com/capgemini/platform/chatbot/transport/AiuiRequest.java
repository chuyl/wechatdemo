package com.capgemini.platform.chatbot.transport;

/**
 * @author Gavin
 * @date 2018/5/22.
 */
public class AiuiRequest {
    private String question;
    private String dataType;
    private String userCode;
    private String fileName;
    private String scene;

    public AiuiRequest() {
    }

    public AiuiRequest(String question, String dataType, String userCode, String fileName,String scene) {
        this.question = question;
        this.dataType = dataType;
        this.userCode = userCode;
        this.fileName = fileName;
        this.scene = scene;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getScene() {
        return scene;
    }

    public void setScene(String scene) {
        this.scene = scene;
    }

    @Override
    public String toString() {
        return "AiuiRequest{" +
                "question='" + question + '\'' +
                ", dataType='" + dataType + '\'' +
                ", userCode='" + userCode + '\'' +
                ", fileName='" + fileName + '\'' +
                ", scene='" + scene + '\'' +
                '}';
    }
}
