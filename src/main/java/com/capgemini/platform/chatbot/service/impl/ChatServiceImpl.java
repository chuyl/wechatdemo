package com.capgemini.platform.chatbot.service.impl;

import com.capgemini.platform.chatbot.dao.*;
import com.capgemini.platform.chatbot.model.*;
import com.capgemini.platform.chatbot.service.AiuiService;
import com.capgemini.platform.chatbot.service.BaseService;
import com.capgemini.platform.chatbot.service.ChatService;
import com.capgemini.platform.chatbot.transport.AiuiRequest;
import com.capgemini.platform.chatbot.transport.AiuiResponse;
import com.capgemini.platform.core.util.AIUIUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 *
 * @Author:yanlchu
 * @Date:12/4/2018
 */
@Service
public class ChatServiceImpl extends BaseService implements ChatService {


    @Autowired
    private QuestionCategoryMapper questionCategoryMapper;
    @Autowired
    private ChatHistoryMapper chatHistoryMapper;
    @Autowired
    private QuestionCategoryMapperExt questionCategoryMapperExt;
    @Autowired
    private QuestionTopicMapperExt questionTopicMapperExt;
    @Autowired
    private AiuiService aiuiService;
    @Autowired
    private UnansweredQuestionMapper unansweredQuestionMapper;

    @Override
    public ResponseEntity<Chat> chat(Chat chat) throws Exception{

        /**
         * 把当前提问保存到聊天记录中
         * 0: text 1: audio 2: image
         */
        ChatHistory chatHistory = new ChatHistory();
        chatHistory.setDataType(chat.getDataType());
        chatHistory.setQuestion(chat.getQuestion());
        chatHistory.setUserCode(chat.getUserCode());

        boolean hasAnswer = false;

        if (StringUtils.isBlank(chat.getQuestion())||(!hasAnswer&&"0".equals(chat.getQuestion())
                &&chat.getPid()==1L)){
            List<QuestionCategory> list= questionCategoryMapperExt.getListByParentIdWithoutNa(1L);
            StringBuilder sb = new StringBuilder();
            sb.append("您好，请问您有什么问题：");
            sb.append("\n");
            sb= linkCategory(sb,list);
            chat.setAnswer(sb.toString());
            chat.setPid(1L);
            chat.setScene("main");
            hasAnswer = true;
        }

        if(!hasAnswer&&"0".equals(chat.getQuestion())&&chat.getPid()!=null&&chat.getPid()!=1L){
            List<QuestionCategory> list= questionCategoryMapperExt.getParentList(chat.getPid());
            if(list!=null && list.size()>0){
                StringBuilder sb = new StringBuilder();
                sb= linkCategory(sb,list);
                chat.setAnswer(sb.toString());
                chat.setPid(list.get(0).getParentId());
                hasAnswer= true;
            }
        }

        //用户选择非0的答案
        Long qqqq=chat.getPid();
        if(!hasAnswer&&(!"0".equals(chat.getQuestion()))&&chat.getPid()!=null){
            //通过问题code与parentId找到当前的id，用来下面找下层的问题
            QuestionCategory qct= questionCategoryMapperExt.getNewLevelId(chat.getQuestion(),chat.getPid());
            if(qct!=null){
                Long currentId=qct.getId();
                qqqq= currentId;
                List<QuestionCategory> list= questionCategoryMapperExt.getListByParentIdWithoutNa(currentId);
                if(list!=null && list.size()>0){
                    StringBuilder sb = new StringBuilder();
                    sb= linkCategory(sb,list);
                    chat.setAnswer(sb.toString());
                    chat.setPid(currentId);
                    chat.setScene(qct.getScene());
                    hasAnswer= true;
                }else{//找不到的话说明在最下层，或者直接输入的问题
                    List<QuestionTopic> topicList= questionTopicMapperExt.getTopicListByCategory(currentId,true);
                    if(topicList!=null&&topicList.size()>0){
                        String topicAnswer= linkTopic(topicList);
                        chat.setAnswer(topicAnswer);
                        chat.setPid(currentId);
                        chat.setScene(qct.getScene());
                        hasAnswer=true;
                    }
                }
            }else{//找不到的话去topic里去找
                QuestionTopic questionTopic= questionTopicMapperExt.getQuestionTopic(chat.getQuestion(),chat.getPid());
                if(questionTopic!=null){
                    chat.setQuestion(questionTopic.getTopic());
                }
            }
        }

        //如果没有结果，返回通用回答

        if(!hasAnswer){
            /**
             * 判断提问方式：语音或文本
             * */
            AiuiRequest request = new AiuiRequest();
            request.setQuestion(chat.getQuestion());
            request.setDataType("text");
            request.setUserCode(chat.getUserCode());
            request.setScene(chat.getScene()!=null?chat.getScene(): AIUIUtil.SCENE);
            AiuiResponse response = aiuiService.searchAnswer(request);
            if("0".equals(response.getCode())) {
                //成功找到答案
                chat.setAnswer(response.getData());
            }else{
                List<QuestionCategory> list= questionCategoryMapperExt.getCommonAnswerByParentId(qqqq);
                if (list!=null&&list.size()>0){
                    chat.setAnswer(list.get(0).getCategoryName());
                }else{
                    chat.setAnswer("嗨，您这条问题太难了，请输入\"转人工\"联系人工客服！");
                }
                UnansweredQuestion unansweredQuestion= new UnansweredQuestion();
                unansweredQuestion.setCategoryId(chat.getPid());
                unansweredQuestion.setQuestion(chat.getQuestion());
                unansweredQuestion.setDeletedFlag(0L);
                unansweredQuestion.setCreatedBy(chat.getUserCode());
                unansweredQuestion.setCreatedOn(new Date());
                unansweredQuestionMapper.insert(unansweredQuestion);
                //找不到答案  记录下问题，和问题分类
            }
        }
        chatHistory.setAnswer(chat.getAnswer());
        chatHistory.setCreatedOn(new Date());
        chatHistory.setModifiedOn(new Date());
        chatHistory.setCategoryId(chat.getPid());
        chatHistoryMapper.insert(chatHistory);

       return new ResponseEntity<Chat>(chat, HttpStatus.CREATED);
    }

    private String linkTopic(List<QuestionTopic> topicList) {
        StringBuilder sb =new StringBuilder();
        topicList.forEach(a->{
            sb.append(a.getCode());
            sb.append(".");
            sb.append(a.getTopic());
            sb.append("\n");
        });
        sb.append("0.返回上一层");
        sb.append("\n");
        sb.append("\n");
        sb.append("如果没有您想咨询的内容，请输入\"人工客服\"为您解答");
        sb.append("\n");
        return sb.toString();
    }

   
    private StringBuilder linkCategory(StringBuilder sb,List<QuestionCategory> list){

        list.forEach(a->{
            sb.append(a.getCode());
            sb.append(".");
            sb.append(a.getCategoryName());
            sb.append("\n");
        });
        sb.append("\n");
        sb.append("如果没有您想咨询的内容，请输入\"人工客服\"为您解答");
        sb.append("\n");
        return sb;
    }
    
    
}
