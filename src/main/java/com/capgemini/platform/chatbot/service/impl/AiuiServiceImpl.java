package com.capgemini.platform.chatbot.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.capgemini.platform.chatbot.transport.AiuiRequest;
import com.capgemini.platform.chatbot.transport.AiuiResponse;
import com.capgemini.platform.chatbot.service.AiuiService;
import com.capgemini.platform.core.util.AIUIUtil;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Gavin
 * @date 2018/5/22.
 */
@Service
public class AiuiServiceImpl implements AiuiService {
    @Override
    public AiuiResponse searchAnswer(AiuiRequest request) throws Exception{
        String dataType = request.getDataType();
        byte[] bQuestion = "".getBytes("UTF-8");
        Map<String, String> header = new HashMap<>();

        //加上文字与声音处理逻辑
        if("text".equals(dataType)){
            String question  = request.getQuestion();
            bQuestion = question.getBytes("UTF-8");

            header = AIUIUtil.buildTextHeader(request.getScene());

            String result = AIUIUtil.httpPost(AIUIUtil.URL, header, bQuestion);
            AiuiResponse response = new AiuiResponse();
            JSONObject json = JSON.parseObject(result);
            String code = (String) json.get("code");
            response.setCode(code);
            String desc = (String) json.get("desc");
            response.setDesc(desc);
            String sid = (String) json.get("sid");
            response.setSid(sid);
            JSONArray datas = (JSONArray) json.get("data");

            if(datas.size() > 0){
                JSONObject data = (JSONObject)datas.get(0);
                JSONObject intent = (JSONObject)data.get("intent");
                JSONObject answer = (JSONObject)intent.get("answer");
                if(answer != null) {
                    String text = (String) answer.get("text");
                    response.setData(text);
                }else {
                    //20005 无匹配结果
                    response.setCode("20005");
                    response.setDesc("无匹配结果");
                }
            }

            System.out.println(request.toString());
            System.out.println(result);
            return response;

        }else {
            bQuestion = AIUIUtil.readFile(request.getFileName());

            header = AIUIUtil.buildAudioHeader(request.getScene());

            String result = AIUIUtil.httpPost(AIUIUtil.URL, header, bQuestion);
            AiuiResponse response = new AiuiResponse();
            JSONObject json = JSON.parseObject(result);
            String code = (String) json.get("code");
            response.setCode(code);
            String desc = (String) json.get("desc");
            response.setDesc(desc);
            String sid = (String) json.get("sid");
            response.setSid(sid);
            JSONArray datas = (JSONArray) json.get("data");

            for (int i = 0; i < datas.size(); i++) {
                JSONObject data = (JSONObject)datas.get(i);
                JSONObject intent = (JSONObject)data.get("intent");
                if(intent != null && intent.size()>0){
                    String text = (String) intent.get("text");
                    response.setAsk(text);
                    JSONObject answer = (JSONObject) intent.get("answer");
                    if(answer != null && answer.size()>0){
                        String txtAnswer = (String) answer.get("text");
                        response.setData(txtAnswer);
                    }else {
                        continue;
                    }

                }else {
                    continue;
                }
            }

            if(null == response.getData()){
                //20005 无匹配结果
                response.setCode("20005");
                response.setDesc("无匹配结果");
            }

            return response;
        }
    }
}
