package com.capgemini.platform.chatbot.service;

import com.capgemini.platform.chatbot.transport.AiuiRequest;
import com.capgemini.platform.chatbot.transport.AiuiResponse;

/**
 * 实现对科大讯飞的接口服务
 * @author Gavin
 * @date 2018/5/22.
 */
public interface AiuiService {
    /**
     * 实现对科大讯飞的接口服务
     * @param request
     * @return
     */
    AiuiResponse searchAnswer(AiuiRequest request)  throws Exception;
}
