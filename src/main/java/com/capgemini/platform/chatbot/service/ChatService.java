package com.capgemini.platform.chatbot.service;

import com.capgemini.platform.chatbot.model.Chat;
import org.springframework.http.ResponseEntity;

/**
 *
 * @Author:yanlchu
 * @Date:12/4/2018
 */
public interface ChatService {

    ResponseEntity<Chat> chat(Chat chat) throws Exception;

}
