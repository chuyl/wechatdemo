package com.capgemini.platform.chatbot.service;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public abstract class BaseService {

    protected ResponseEntity success(){
        return new ResponseEntity(HttpStatus.OK);
    }

    protected ResponseEntity success(Object object){
        return new ResponseEntity(object,HttpStatus.OK);
    }

    protected ResponseEntity success(Object object,HttpStatus status){
        return new ResponseEntity(object,status);
    }

//    protected ResponseEntity<T> fail(T body, HttpStatus status){
//        return new ResponseEntity(body,  status);
//    }

    protected ResponseEntity fail(String str, HttpStatus status){
        return new ResponseEntity(str,  status);
    }
    protected ResponseEntity fail( HttpStatus status){
        return new ResponseEntity(status);
    }
}
