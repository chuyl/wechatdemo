package com.capgemini.platform.chatbot.dao;

import com.capgemini.platform.chatbot.model.QuestionTopic;
import com.capgemini.platform.chatbot.transport.QuestionTopicRequest;
import com.capgemini.platform.chatbot.transport.QuestionTopicResponse;
import com.capgemini.platform.core.support.MyMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface QuestionTopicMapperExt extends MyMapper<QuestionTopic> {
    List<QuestionTopic> getTopicListByCategory(@Param("categoryId") Long categoryId,@Param("shown") Boolean shown);

    QuestionTopic getQuestionTopic(@Param("question")String question, @Param("pid")Long pid);

    List<QuestionTopicResponse> search(@Param("request")QuestionTopicRequest request);

}