package com.capgemini.platform.chatbot.dao;

import com.capgemini.platform.chatbot.model.QuestionList;
import com.capgemini.platform.chatbot.transport.QuestionListResponse;
import com.capgemini.platform.chatbot.transport.QuestionTopicRequest;
import com.capgemini.platform.core.support.MyMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface QuestionListMapperExt extends MyMapper<QuestionList> {
    List<QuestionList> getListByTopicId(@Param("id") Long id);

    List<QuestionListResponse> search(@Param("request")QuestionTopicRequest request);
}