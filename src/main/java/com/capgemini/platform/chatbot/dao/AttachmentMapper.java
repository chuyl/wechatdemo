package com.capgemini.platform.chatbot.dao;

import com.capgemini.platform.chatbot.model.Attachment;
import com.capgemini.platform.core.support.MyMapper;

public interface AttachmentMapper extends MyMapper<Attachment> {
}