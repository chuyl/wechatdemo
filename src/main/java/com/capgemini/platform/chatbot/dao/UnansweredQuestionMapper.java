package com.capgemini.platform.chatbot.dao;

import com.capgemini.platform.chatbot.model.UnansweredQuestion;
import com.capgemini.platform.core.support.MyMapper;
import org.springframework.stereotype.Component;

@Component
public interface UnansweredQuestionMapper extends MyMapper<UnansweredQuestion> {
}