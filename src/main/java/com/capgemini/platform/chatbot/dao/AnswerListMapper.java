package com.capgemini.platform.chatbot.dao;

import com.capgemini.platform.chatbot.model.AnswerList;
import com.capgemini.platform.core.support.MyMapper;
import org.springframework.stereotype.Component;

@Component
public interface AnswerListMapper extends MyMapper<AnswerList> {
}