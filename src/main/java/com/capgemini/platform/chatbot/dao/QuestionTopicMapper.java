package com.capgemini.platform.chatbot.dao;

import com.capgemini.platform.chatbot.model.QuestionTopic;
import com.capgemini.platform.core.support.MyMapper;
import org.springframework.stereotype.Component;

@Component
public interface QuestionTopicMapper extends MyMapper<QuestionTopic> {
}