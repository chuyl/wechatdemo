package com.capgemini.platform.chatbot.dao;

import com.capgemini.platform.chatbot.model.ChatNoAnswerHistory;
import com.capgemini.platform.core.support.MyMapper;

public interface ChatNoAnswerHistoryMapper extends MyMapper<ChatNoAnswerHistory> {
}