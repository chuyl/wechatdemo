package com.capgemini.platform.chatbot.dao;

import com.capgemini.platform.chatbot.model.AnswerList;
import com.capgemini.platform.chatbot.transport.AnswerListResponse;
import com.capgemini.platform.chatbot.transport.QuestionTopicRequest;
import com.capgemini.platform.core.support.MyMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface AnswerListMapperExt extends MyMapper<AnswerList> {

    List<AnswerList> getListByTopicId(@Param("id") Long id);

    List<AnswerListResponse> search(@Param("request")QuestionTopicRequest request);
}