package com.capgemini.platform.chatbot.dao;

import com.capgemini.platform.chatbot.model.ChatHistory;
import com.capgemini.platform.core.support.MyMapper;
import org.springframework.stereotype.Component;

@Component
public interface ChatHistoryMapper extends MyMapper<ChatHistory> {
}