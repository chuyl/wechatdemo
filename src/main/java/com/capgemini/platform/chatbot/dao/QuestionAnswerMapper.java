package com.capgemini.platform.chatbot.dao;

import com.capgemini.platform.chatbot.model.QuestionAnswer;
import com.capgemini.platform.core.support.MyMapper;

public interface QuestionAnswerMapper extends MyMapper<QuestionAnswer> {
}