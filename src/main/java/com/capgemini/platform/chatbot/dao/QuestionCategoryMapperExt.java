package com.capgemini.platform.chatbot.dao;

import com.capgemini.platform.chatbot.model.QuestionCategory;
import com.capgemini.platform.chatbot.transport.QuestionCategoryRequest;
import com.capgemini.platform.chatbot.transport.QuestionCategoryResponse;
import com.capgemini.platform.core.support.MyMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface QuestionCategoryMapperExt extends MyMapper<QuestionCategory> {
    List<QuestionCategory> getListByParentId(@Param("id") Long id);

    List<QuestionCategory> getParentList(@Param("pid")Long pid);

    QuestionCategory getNewLevelId(@Param("question")String question, @Param("pid")Long pid);

    List<QuestionCategory> getCommonAnswerByParentId( @Param("pid")Long pid);

    List<QuestionCategoryResponse> search(@Param("request")QuestionCategoryRequest request);

    List<QuestionCategory> getListByParentIdWithoutNa(@Param("id")Long currentId);
}