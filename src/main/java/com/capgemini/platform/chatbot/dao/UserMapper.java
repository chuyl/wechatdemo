package com.capgemini.platform.chatbot.dao;

import com.capgemini.platform.chatbot.model.User;
import com.capgemini.platform.core.support.MyMapper;

public interface UserMapper extends MyMapper<User> {
}