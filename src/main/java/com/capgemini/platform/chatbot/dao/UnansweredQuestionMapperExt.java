package com.capgemini.platform.chatbot.dao;

import com.capgemini.platform.chatbot.model.UnansweredQuestion;
import com.capgemini.platform.chatbot.transport.UnansweredQuestionRequest;
import com.capgemini.platform.chatbot.transport.UnansweredQuestionResponse;
import com.capgemini.platform.core.support.MyMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface UnansweredQuestionMapperExt extends MyMapper<UnansweredQuestion> {
    List<UnansweredQuestionResponse> search(@Param("request") UnansweredQuestionRequest request);
}