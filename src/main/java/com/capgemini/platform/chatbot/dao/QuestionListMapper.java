package com.capgemini.platform.chatbot.dao;

import com.capgemini.platform.chatbot.model.QuestionList;
import com.capgemini.platform.core.support.MyMapper;
import org.springframework.stereotype.Component;

@Component
public interface QuestionListMapper extends MyMapper<QuestionList> {
}