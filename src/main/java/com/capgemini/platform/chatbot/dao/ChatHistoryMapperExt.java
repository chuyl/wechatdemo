package com.capgemini.platform.chatbot.dao;

import com.capgemini.platform.chatbot.model.ChatHistory;
import com.capgemini.platform.chatbot.transport.ChatHistoryRequest;
import com.capgemini.platform.chatbot.transport.QuestionCategoryResponse;
import com.capgemini.platform.core.support.MyMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface ChatHistoryMapperExt extends MyMapper<ChatHistory> {
    List<QuestionCategoryResponse> search(@Param("request") ChatHistoryRequest request);
}