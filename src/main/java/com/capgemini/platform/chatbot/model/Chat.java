package com.capgemini.platform.chatbot.model;

public class Chat {

    /**
     * 用户输入的或系统加工后的问题
     */
    private String question;

    /**
     * 系统回答或讯飞回答
     */
    private String answer;

    private Integer dataType;

    private String userCode;

    private Long attachId;

    private String pcode;

    private String pcodes;

    private Long pid;

    private String scene;

    public Chat() {
    }

    public Chat(String question, String answer, Integer dataType, String userCode, Long attachId, String pcode, String pcodes) {
        this.question = question;
        this.answer = answer;
        this.dataType = dataType;
        this.userCode = userCode;
        this.attachId = attachId;
        this.pcode = pcode;
        this.pcodes = pcodes;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public Integer getDataType() {
        return dataType;
    }

    public void setDataType(Integer dataType) {
        this.dataType = dataType;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public Long getAttachId() {
        return attachId;
    }

    public void setAttachId(Long attachId) {
        this.attachId = attachId;
    }

    public String getPcode() {
        return pcode;
    }

    public void setPcode(String pcode) {
        this.pcode = pcode;
    }

    public String getPcodes() {
        return pcodes;
    }

    public void setPcodes(String pcodes) {
        this.pcodes = pcodes;
    }

    public Long getPid() {
        return pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }

    public String getScene() {
        return scene;
    }

    public void setScene(String scene) {
        this.scene = scene;
    }
}