package com.capgemini.platform.chatbot.model;

import javax.persistence.*;
import java.util.Date;

@Table(name = "chatbot_dictionary")
public class Dictionary {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT LAST_INSERT_ID()")
    private Long id;

    private String code;

    private String name;

    @Column(name = "gmt_created")
    private Date gmtCreated;

    @Column(name = "gmt_modified")
    private Date gmtModified;

    private String pcode;

    private String pcodes;

    private Integer num;

    public Dictionary() {
    }

    public Dictionary(Long id, String code, String name, String pcode, String pcodes, Date gmtCreated, Date gmtModified, Integer num) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.pcode = pcode;
        this.pcodes = pcodes;
        this.gmtCreated = gmtCreated;
        this.gmtModified = gmtModified;
        this.num = num;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getGmtCreated() {
        return gmtCreated;
    }

    public void setGmtCreated(Date gmtCreated) {
        this.gmtCreated = gmtCreated;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    public String getPcode() {
        return pcode;
    }

    public void setPcode(String pcode) {
        this.pcode = pcode;
    }

    public String getPcodes() {
        return pcodes;
    }

    public void setPcodes(String pcodes) {
        this.pcodes = pcodes;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    @Override
    public String toString() {
        return "Dictionary{" +
                "id=" + id +
                ", code='" + code + '\'' +
                ", name='" + name + '\'' +
                ", gmtCreated=" + gmtCreated +
                ", gmtModified=" + gmtModified +
                ", pcode='" + pcode + '\'' +
                ", pcodes='" + pcodes + '\'' +
                ", num=" + num +
                '}';
    }
}