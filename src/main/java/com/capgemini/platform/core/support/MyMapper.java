package com.capgemini.platform.core.support;

import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

/**
 * 继承自己的MyMapper
 * @author Gavin
 * @date 2018/4/29.
 */
@tk.mybatis.mapper.annotation.RegisterMapper
public interface MyMapper<T> extends Mapper<T>, MySqlMapper<T> {
//FIXME 特别注意，该接口不能被扫描到，否则会出错
}
