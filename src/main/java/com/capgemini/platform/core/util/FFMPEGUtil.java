package com.capgemini.core.util;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * 封装的转码工具类
 * @author Gavin
 * @date 2018/5/28.
 */
public class FFMPEGUtil {
    public static String silk2Pcm(String inputfile,String outputfile){
        List<String> commend = new ArrayList<String>();
        commend.add("/usr/local/silk2pcm_tool/SILKCodec/SILK_SDK_SRC_ARM/./decoder");
        commend.add(inputfile);
        commend.add(outputfile);
        StringBuffer test=new StringBuffer();
        for(int i=0;i<commend.size();i++)
            test.append(commend.get(i)+" ");
        System.out.println("decoder命令:"+test+"");
        exec(test);
        return outputfile;
    }
    public static String pcm2Wav(String inputfile,String outputfile){
        //ffmpeg -f s16le -ar 12k -ac 2 -i /path/to/pcm -f wav -ar 16k -ac 1 /path/to/wav
        List<String> commend = new ArrayList<String>();
        commend.add("ffmpeg");
        commend.add("-f");
        commend.add("s16le");
        commend.add("-ar");
        commend.add("12k");
        commend.add("-ac");
        commend.add("2");
        commend.add("-i");
        commend.add(inputfile);
        commend.add("-f");
        commend.add("wav");
        commend.add("-ar");
        commend.add("16k");
        commend.add("-ac");
        commend.add("1");
        commend.add(outputfile);
        StringBuffer test=new StringBuffer();
        for(int i=0;i<commend.size();i++)
            test.append(commend.get(i)+" ");
        System.out.println("ffmpeg命令:"+test+"");
        exec(test);
        return outputfile;
    }

    public static String silk_remove_word(String filepath){
        List<String> commend = new ArrayList<String>();
        commend.add("/home/workspace/./test.sh");
        commend.add(filepath);
        StringBuffer test=new StringBuffer();
        for(int i=0;i<commend.size();i++)
            test.append(commend.get(i)+" ");
        System.out.println("test命令:"+test+"");
        exec(test);
        return filepath;
    }

    private static void exec(StringBuffer test){
        try {
            Runtime rt = Runtime.getRuntime();
            Process proc = rt.exec(test.toString());
            InputStream stderr = proc.getErrorStream();
            InputStreamReader isr = new InputStreamReader(stderr);
            BufferedReader br = new BufferedReader(isr);
            String line = null;
            while ( (line = br.readLine()) != null) ;

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
