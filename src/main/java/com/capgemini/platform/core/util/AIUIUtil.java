package com.capgemini.platform.core.util;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

/**
 * 科大讯飞工具类
 * @author Gavin
 * @date 2018/5/23.
 */
public class AIUIUtil {
    public static final String URL =  "http://openapi.xfyun.cn/v2/aiui";
    private static final String APPID = "5b0264e8";
    private static final String API_KEY = "9a06388e65104fffa091b189d1e3770e";
    public static final String SCENE = "main";
    private static final String AUTH_ID = "2894c985bf8b1111c6728db79d3479ae";

    public static void main(String[] args) throws IOException,ParseException, InterruptedException{
        final String FILE_PATH = "";
        Map<String, String> header = buildTextHeader(SCENE);
        byte[] dataByteArray = readFile(FILE_PATH);
        String result = httpPost(URL, header, dataByteArray);
        System.out.println(result);
    }

    public static Map<String, String> buildTextHeader(String scene) throws UnsupportedEncodingException, ParseException {
        final String DATA_TYPE = "text";
        final String AUE = "raw";
        final String SAMPLE_RATE = "16000";
        String curTime = System.currentTimeMillis() / 1000L + "";
        String param = "{\"aue\":\""+AUE+"\",\"sample_rate\":\""+SAMPLE_RATE+"\",\"auth_id\":\""+AUTH_ID+"\",\"data_type\":\""+DATA_TYPE+"\",\"scene\":\""+scene+"\"}";

        String paramBase64 = new String(Base64.encodeBase64(param.getBytes("UTF-8")));
        String checkSum = DigestUtils.md5Hex(API_KEY + curTime + paramBase64);

        Map<String, String> header = new HashMap<String, String>();
        header.put("X-Param", paramBase64);
        header.put("X-CurTime", curTime);
        header.put("X-CheckSum", checkSum);
        header.put("X-Appid", APPID);
        return header;
    }

    public static Map<String, String> buildAudioHeader(String scene) throws UnsupportedEncodingException, ParseException {
        final String DATA_TYPE = "audio";
        final String AUE = "wav";
        final String SAMPLE_RATE = "16000";
        final String RESULT_LEVEL = "complete";
        final String LAT = "39.938838";
        final String LNG = "116.368624";

        String curTime = System.currentTimeMillis() / 1000L + "";
        String param = "{\"result_level\":\""+RESULT_LEVEL+"\",\"auth_id\":\""+AUTH_ID+"\",\"data_type\":\""+DATA_TYPE+"\",\"sample_rate\":\""+SAMPLE_RATE+"\",\"scene\":\""+scene+"\",\"lat\":\""+LAT+"\",\"lng\":\""+LNG+"\"}";

        String paramBase64 = new String(Base64.encodeBase64(param.getBytes("UTF-8")));
        String checkSum = DigestUtils.md5Hex(API_KEY + curTime + paramBase64);

        Map<String, String> header = new HashMap<String, String>();
        header.put("X-Param", paramBase64);
        header.put("X-CurTime", curTime);
        header.put("X-CheckSum", checkSum);
        header.put("X-Appid", APPID);
        return header;
    }

    public static byte[] readFile(String filePath) throws IOException {
        InputStream in = new FileInputStream(filePath);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024 * 4];
        int n = 0;
        while ((n = in.read(buffer)) != -1) {
            out.write(buffer, 0, n);
        }
        byte[] data = out.toByteArray();
        in.close();
        return data;
    }

    public static String httpPost(String url, Map<String, String> header, byte[] body) {
        String result = "";
        BufferedReader in = null;
        OutputStream out = null;
        try {
            URL realUrl = new URL(url);
            HttpURLConnection connection = (HttpURLConnection)realUrl.openConnection();
            for (String key : header.keySet()) {
                connection.setRequestProperty(key, header.get(key));
            }
            connection.setDoOutput(true);
            connection.setDoInput(true);

            //connection.setConnectTimeout(20000);
            //connection.setReadTimeout(20000);
            try {
                out = connection.getOutputStream();
                out.write(body);
                out.flush();
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String line;
                while ((line = in.readLine()) != null) {
                    result += line;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
